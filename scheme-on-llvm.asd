(defsystem "scheme-on-llvm"
  :version "0.1.0"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("fcl" "cl-ppcre")
  :components ((:module "src"
                :components
                ((:file "util")
                 (:file "queue"          :depends-on ("util"))
                 (:file "lexer"          :depends-on ("util"))
                 (:file "parser"         :depends-on ("util" "queue" "lexer"))
                 (:file "code-generator" :depends-on ("util" "parser"))
                 (:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "scheme-on-llvm/tests"))))

(defsystem "scheme-on-llvm/tests"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("scheme-on-llvm"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for scheme-on-llvm"
  :perform (test-op (op c) (symbol-call :rove :run c)))
