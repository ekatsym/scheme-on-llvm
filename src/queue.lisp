(defpackage scheme-on-llvm.queue
  (:use :common-lisp :scheme-on-llvm.util)
  (:import-from :fcl #:defdata #:data= #:match #:ematch)
  (:export
    #:queue
    #:empty
    #:empty?
    #:enqueue
    #:qfirst
    #:qrest
    #:list->queue
    #:queue->list))
(in-package :scheme-on-llvm.queue)


;;; Queue Definition
(defdata queue
  (%queue integer list integer list))

(defmethod print-object ((object %queue) stream)
  (format stream "#<QUEUE ~S>" (queue->list object)))

(defun empty ()
  (%queue 0 '() 0 '()))

(defun empty? (queue)
  (check-type queue queue)
  (match queue
    ((%queue 0 _ _ _) t)
    (_                nil)))

(defun check-queue (queue)
  (ematch queue
    ((%queue len1 head len2 tail)
     (if (>= len1 len2)
         queue
         (%queue (+ len1 len2) (append head (reverse tail)) 0 '())))))

(defun enqueue (x queue)
  (ematch queue
    ((%queue len1 head len2 tail)
     (check-queue (%queue len1 head (1+ len2) (cons x tail))))))

(defun qfirst (queue)
  (ematch queue
    ((%queue _ (cons x _) _ _) x)))

(defun qrest (queue)
  (ematch queue
    ((%queue len1 (cons _ head) len2 tail)
     (check-queue (%queue (1- len1) head len2 tail)))))

(defun queue->list (queue)
  (check-type queue queue)
  (ematch queue
    ((%queue _ head _ tail) (append head (reverse tail)))))

(defun list->queue (list)
  (check-type list list)
  (%queue (length list) (reverse list) 0 '()))

