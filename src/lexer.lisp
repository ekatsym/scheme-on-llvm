(defpackage scheme-on-llvm.lexer
  (:use :common-lisp :scheme-on-llvm.util)
  (:import-from :fcl #:defdata #:ematch #:maybe #:nothing #:just #:mlet)
  (:export
    #:lex #:token
    #:token-identifier
    #:token-boolean
    #:token-number
    #:token-character
    #:token-string
    #:token-paren-open
    #:token-paren-close
    #:token-hash-paren-open
    #:token-quote
    #:token-back-quote
    #:token-comma
    #:token-comma-at
    #:token-dot
    #:token-eof))
(in-package :scheme-on-llvm.lexer)


;;; Token Definition
(defdata token
  (token-identifier string)
  (token-boolean string)
  (token-number string)
  (token-character string)
  (token-string string)
  (token-paren-open)
  (token-paren-close)
  (token-hash-paren-open)
  (token-quote)
  (token-back-quote)
  (token-comma)
  (token-comma-at)
  (token-dot)
  (token-eof))

;;; Regex Patterns
(defparameter *whitespace*
  '(:alternation #\space #\newline))

(defparameter *delimiter*
  `(:alternation ,*whitespace* #\( #\) #\" #\;))

(defparameter *comment*
  '(:sequence #\; (:greedy-repetition 0 nil :everything) #\newline))

(defparameter *atmosphere*
  `(:alternation ,*whitespace* ,*comment*))

(defparameter *intertoken-space*
  `(:greedy-repetition 0 nil ,*atmosphere*))

(defparameter *letter*
  '(:char-class (:range #\a #\z)))

(defparameter *digit*
  '(:char-class (:range #\0 #\9)))

(defparameter *special-initial*
  '(:alternation #\! #\$ #\% #\& #\* #\/ #\: #\< #\= #\> #\? #\^ #\_ #\~))

(defparameter *initial*
  `(:alternation ,*letter* ,*special-initial*))

(defparameter *special-subsequent*
  `(:alternation #\+ #\- #\. #\@))

(defparameter *subsequent*
  `(:alternation ,*initial* ,*digit* ,*special-subsequent*))

(defparameter *peculiar-identifier*
  '(:alternation #\+ #\-))

(defparameter *identifier*
  `(:alternation
     (:sequence ,*initial* (:greedy-repetition 0 nil ,*subsequent*))
     ,*peculiar-identifier*))

(defparameter *boolean*
  '(:alternation (:sequence #\# #\t) (:sequence #\# #\f)))

(defparameter *character-name*
  '(:alternation "space" "newline"))

(defparameter *character*
  `(:sequence #\# #\\ (:alternation ,*character-name* :everything)))

(defparameter *string-element*
  '(:alternation
     (:inverted-char-class #\" #\\)
     (:sequence #\# #\")
     (:sequence #\# #\\)))

(defparameter *string*
  `(:sequence #\" (:greedy-repetition 0 nil ,*string-element*) #\"))

(defparameter *sign*
  '(:alternation :void #\+ #\-))

(defparameter *exponent-marker*
  '(:alternation #\e #\s #\f #\d #\l))

(defun digit (r)
  (ecase r
    (2 '(:char-class (:range #\0 #\1)))
    (8 '(:char-class (:range #\0 #\7)))
    (10 *digit*)
    (16 `(:alternation ,*digit* (:char-class (:range #\a #\f))))))

(defparameter *suffix*
  `(:alternation
     :void
     (:sequence ,*exponent-marker* ,*sign* (:greedy-repetition 1 nil ,(digit 10)))))

(defparameter *exactness*
  `(:alternation :void "#i" "#e"))

(defun radix (r)
  (ecase r
    (2 "#b")
    (8 "#o")
    (10 '(:alternation :void (:sequence #\# #\d)))
    (16 "#x")))

(defun num (r)
  `(:sequence ,(num-prefix r) ,(num-complex r)))

(defun num-complex (r)
  `(:alternation
     ,(num-real r)
     (:sequence ,(num-real r) #\@ ,(num-real r))
     (:sequence ,(num-real r) #\+ ,(num-ureal r) #\i)
     (:sequence ,(num-real r) #\- ,(num-ureal r) #\i)
     (:sequence ,(num-real r) #\+ #\i)
     (:sequence ,(num-real r) #\- #\i)
     (:sequence #\+ ,(num-ureal r) #\i)
     (:sequence #\- ,(num-ureal r) #\i)
     "+i" "-i"))

(defun num-real (r)
  `(:sequence ,*sign* ,(num-ureal r)))

(defun num-ureal (r)
  (if (= r 10)
      `(:alternation
         ,(num-decimal r) 
         ,(num-uinteger r)
         (:sequence ,(num-uinteger r) #\/ ,(num-uinteger r)))
      `(:alternation
         ,(num-uinteger r)
         (:sequence ,(num-uinteger r) #\/ ,(num-uinteger r)))))

(defun num-decimal (r)
  (assert (= r 10) (r))
  `(:alternation
     (:sequence
       (:greedy-repetition 1 nil ,(digit 10))
       #\. (:greedy-repetition 0 nil ,(digit 10))
       ,*suffix*)
     (:sequence
       #\. (:greedy-repetition 1 nil ,(digit 10))
       ,*suffix*)
     (:sequence ,(num-uinteger 10) ,*suffix*)))

(defun num-uinteger (r)
  `(:sequence (:greedy-repetition 1 nil ,(digit r)) (:greedy-repetition 0 nil #\#)))

(defun num-prefix (r)
  `(:alternation
     (:sequence ,(radix r) ,*exactness*)
     (:sequence ,*exactness* ,(radix r))))

(defparameter *num*
  `(:alternation ,(num 2) ,(num 8) ,(num 10) ,(num 16)))

;;; Lexer
(defun lex (source)
  (%lex "" source))

(defun %lex (code source)
  (declare (optimize (space 3)))
  (cond ((and (token? *identifier* code) (delimited? source))
         (token-identifier code))
        ((token? *boolean* code)
         (token-boolean code))
        ((and (token? *num* code) (delimited? source))
         (token-number code))
        ((and (token? *character* code) (delimited? source))
         (token-character code))
        ((token? *string* code)
         (token-string (subseq code 1 (1- (length code)))))
        ((token? #\( code)
         (token-paren-open))
        ((token? #\) code)
         (token-paren-close))
        ((token? '(:sequence #\# #\() code)
         (token-hash-paren-open))
        ((token? #\' code)
         (token-quote))
        ((token? #\` code)
         (token-back-quote))
        ((and (token? #\, code) (char/= (peek-char nil source nil #\nul) #\@))
         (token-comma))
        ((token? '(:sequence #\, #\@) code)
         (token-comma-at))
        ((and (token? #\. code) (delimited? source))
         (token-dot))
        ((and (token? :void code) (eof? source))
         (token-eof))
        ((token? *intertoken-space* code)
         (%lex (add-char "" source) source))
        (t
         (%lex (add-char code source) source))))

(defun token? (regex code)
  (multiple-value-bind (start end) (cl-ppcre:scan regex code)
    (if (and start end (= start 0) (= end (length code)))
        code
        nil)))

(defun add-char (code source)
  (concatenate
    'string
    code
    (string
      (char-downcase
        (handler-case (read-char source)
          (end-of-file () (error 'lexer-eof :stream source :code code)))))))

(defun delimited? (source)
  (token? *delimiter* (string (peek-char nil source nil #\space))))

(defun eof? (source)
  (eq 'eof (peek-char nil source nil 'eof)))

(define-condition lexer-eof (end-of-file)
  ((code :initarg :code :reader lexer-eof-code))
  (:report (lambda (o s)
             (format s "end of file on~%~2t~S~%with~%~2t~S"
                     (stream-error-stream o)
                     (lexer-eof-code o)))))

#|
(with-input-from-string (source "identifier #t #f 42 -21 3.14 .3 #b0101 #xff #\\s #\\space #\\  #\\newline \"string\" ( ) #( ' ` , ,@ .")
  (dotimes (i 23) (print (lex source))))
|#

