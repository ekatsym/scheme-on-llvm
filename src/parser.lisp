(defpackage scheme-on-llvm.parser
  (:use :common-lisp :scheme-on-llvm.util :scheme-on-llvm.queue :scheme-on-llvm.lexer)
  (:import-from :fcl #:defdata #:data= #:match #:ematch #:mlet #:either #:left #:right #:foldr)
  (:export
    #:expression
    #:expression-variable #:expression-boolean #:expression-number #:expression-character #:expression-string
    #:expression-quoting #:expression-procedure-call #:expression-lambda-expression
    #:expression-conditional #:expression-assignment #:expression-sequential #:expression-derived-expression
    #:expression-macro-use #:expression-macro-block
    #:parse))
(in-package :scheme-on-llvm.parser)


;;; Datum Definition
(defdata datum
  (datum-boolean string)
  (datum-number string)
  (datum-character string)
  (datum-string string)
  (datum-symbol string)
  (datum-list list)
  (datum-vector list))

;;; Expression Definitions
(defdata expression
  (expression-variable string)
  (expression-boolean string)
  (expression-number string)
  (expression-character string)
  (expression-string string)
  (expression-quoting datum)
  (expression-procedure-call expression list)
  (expression-lambda-expression (or expression-variable list) list list)
  (expression-conditional expression expression expression)
  (expression-assignment expression-variable expression)
  (expression-derived-expression string list)
  (expression-macro-use string list)
  (expression-macro-block boolean list list list))

(defdata definition
  (definition-variable expression-variable expression)
  (definition-function expression-variable list list list)
  (definition-begin list))

;;; Token Seq Definition
(defstruct (token-seq (:constructor make-token-seq (source queue)))
  (source source)
  (queue queue))

(defun take (token-seq)
  (check-type token-seq token-seq)
  (with-slots (source queue) token-seq
    (if (empty? queue)
        (lex source)
        (prog1 (qfirst queue) (setf queue (qrest queue))))))

(defun peek (token-seq)
  (check-type token-seq token-seq)
  (with-slots (source queue) token-seq
    (if (empty? queue)
        (let ((token (lex source)))
          (setf queue (enqueue token queue))
          token)
        (qfirst queue))))

(defun stream->token-seq (stream)
  (check-type stream stream)
  (make-token-seq stream (empty)))

(defun list->token-seq (list)
  (check-type list list)
  (make-token-seq (make-string-input-stream "") (list->queue list)))

;;; Parser
(defun parse (source)
  (datum->expression (parse-datum (stream->token-seq source))))

(defun parse-datum (token-seq)
  (declare (optimize (space 3)))
  (ematch (take token-seq)
    ((token-identifier id)   (datum-symbol id))
    ((token-boolean b)       (datum-boolean b))
    ((token-number n)        (datum-number n))
    ((token-character c)     (datum-character c))
    ((token-string s)        (datum-string s))
    ((token-paren-open)      (datum-list (collect-data token-seq)))
    ((token-paren-close)     (error 'illegal-token-position :token (token-paren-close)))
    ((token-hash-paren-open) (datum-vector (collect-data token-seq)))
    ((token-quote)           (datum-list (list (datum-symbol "quote")
                                               (parse-datum token-seq))))
    ((token-back-quote)      (datum-list (list (datum-symbol "quasiquote")
                                               (parse-datum token-seq))))
    ((token-comma)           (datum-list (list (datum-symbol "unquote")
                                               (parse-datum token-seq))))
    ((token-comma-at)        (datum-list (list (datum-symbol "unquote-splicing")
                                               (parse-datum token-seq))))
    ((token-dot)             (error 'illegal-token-position :token (token-dot)))
    ((token-eof)             (error 'illegal-token-position :token (token-eof)))))

(defun datum->expression (datum)
  (declare (optimize (space 3)))
  (ematch datum
    ((datum-boolean b)             (expression-boolean b))
    ((datum-number n)              (expression-number n))
    ((datum-character c)           (expression-character c))
    ((datum-string s)              (expression-string s))
    ((datum-symbol v)              (expression-variable v))
    ((datum-vector xs)             (expression-quoting xs))
    ((datum-list (cons head tail)) (match head
                                     ((datum-symbol key)
                                      (switch #'string= key
                                        ("quote"
                                         (assert (nlist? 1 tail) (datum))
                                         (expression-quoting (first tail)))
                                        ("lambda"
                                         (destructuring-bind (formals . body) tail
                                           (multiple-value-bind (defs exprs)
                                             (data->definitions+expressions body)
                                             (expression-lambda-expression
                                               (datum->expression/formals formals)
                                               (mapcar #'datum->definition defs)
                                               (mapcar #'datum->expression exprs)))))
                                        ("set!"
                                         (ematch tail
                                           ((list (datum-symbol v) expr)
                                            (expression-assignment
                                              (expression-variable v)
                                              (datum->expression expr)))))
                                        ("if"
                                         (ematch tail
                                           ((list (datum-symbol v) expr)
                                            (expression-assignment
                                              (expression-variable v)
                                              (datum->expression expr)))))
                                        (("cond" "case" "and" "or" "let" "let*" "letrec"
                                          "begin" "do" "delay" "quasiquote")
                                         (datum->expression/derived key tail))
                                        (otherwise
                                         (expression-procedure-call
                                           (datum->expression head)
                                           (mapcar #'datum->expression tail)))))
                                     (_ (expression-procedure-call
                                          (datum->expression head)
                                          (mapcar #'datum->expression tail)))))))

(defun datum->definition (datum)
  (ematch datum
    ((datum-list (list (datum-symbol "define") (datum-symbol name) expr))
     (definition-variable (expression-variable name) expr))
    ((datum-list (cons (datum-symbol "define") (cons (datum-list (cons (datum-symbol name) formals)) body)))
     (multiple-value-bind (defs exprs) (data->definitions+expressions body)
       (definition-function
         (expression-variable name)
         (datum->expression/formals formals)
         (mapcar #'datum->definition defs)
         (mapcar #'datum->expression exprs))))))

(defun datum->expression/formals (formals)
  (declare (optimize (space 3)))
  (if (listp formals)
      (progn
        (assert (every (lambda (formal)
                         (typep formal 'datum-symbol))
                       formals) (formals))
        (mapcar #'datum->expression formals))
      (progn
        (assert (typep formals 'datum-symbol) (formals))
        (datum->expression formals))))

(defun datum->expression/derived (key body)
  (switch #'string= key
    ("cond"       (error 'unsupported))
    ("case"       (error 'unsupported))
    ("and"        (error 'unsupported))
    ("or"         (error 'unsupported))
    ("let"        (ematch body
                    ((cons (datum-list bindings) body)
                     (foldr (lambda (binding body)
                              (ematch (cons binding body)
                                ((cons (datum-list (list (datum-symbol v) expr))
                                       (expression-procedure-call
                                         (expression-lambda-expression formals defs exprs)
                                         args))
                                 (expression-procedure-call
                                   (expression-lambda-expression
                                     (cons (expression-variable v) formals) defs exprs)
                                   (cons (datum->expression expr) args)))))
                            (multiple-value-bind (defs exprs) (data->definitions+expressions body)
                              (expression-procedure-call
                                (expression-lambda-expression
                                  '()
                                  (mapcar #'datum->definition defs)
                                  (mapcar #'datum->expression exprs))
                                '()))
                            bindings))))
    ("let*"       (error 'unsupported))
    ("letrec"     (error 'unsupported))
    ("begin"      (ematch body
                    ((list expr)
                     (datum->expression expr))
                    ((cons expr1 exprs)
                     (datum->expression
                       (datum-list
                         (list (datum-symbol "let")
                               (datum-list
                                 (list (datum-list (list (datum-symbol "_") expr1))))
                               (datum-list
                                 (cons (datum-symbol "begin") exprs))))))))
    ("do"         (error 'unsupported))
    ("delay"      (multiple-value-bind (defs exprs) (data->definitions+expressions body)
                    (expression-lambda-expression '() defs exprs)))
    ("quasiquote" (error 'unsupported))
    (otherwise    (error 'unsupported))))

(defun data->definitions+expressions (data)
  (labels ((rec (defs exprs)
             (declare (optimize (space 3)))
             (match (first exprs)
               ((datum-list (cons (datum-symbol "define") _))
                (rec (cons (first exprs) defs) (rest exprs)))
               (_ (values (reverse defs) exprs)))))
    (rec '() data)))

(defun collect-data (token-seq)
  (declare (optimize (space 3)))
  (labels ((rec (acc)
             (match (peek token-seq)
               ((token-paren-close)
                (take token-seq)
                (reverse acc))
               ((token-dot)
                (prog1 (revappend acc (parse-datum token-seq))
                  (assert (data= (take token-seq) (token-paren-close)))))
               (_ (rec (cons (parse-datum token-seq) acc))))))
    (rec '())))

;;; Conditions
(define-condition illegal-token-position ()
  ((token :initarg :token :reader illegal-token-position-token))
  (:report (lambda (o s) (format s "illegal position: ~S."
                                 (illegal-token-position-token o)))))

(define-condition unsupported () ()
  (:report (lambda (o s)
             (declare (ignore o))
             (format s "unsupported."))))

#|
(with-input-from-string
  (src "(let ((x 0) (y 1)) (+ x y))")
  (parse src))

(with-input-from-string
  (src "(begin (+ 1 1) (* 2 3) (/ 4 2))")
  (parse src))

(with-input-from-string
  (src "(define a 21) (define b 13) (+ a b) (* a b)")
  (let ((tokens (make-token-seq src (empty))))
    (data->definitions+expressions
      (list (parse-datum tokens)
            (parse-datum tokens)
            (parse-datum tokens)
            (parse-datum tokens)))))

(with-input-from-string
  (src (format nil "(define (fact n)~%~2T(if (= n 0)~%~6T1~%~6T(* n (fact (- n 1)))))"))
  (parse-datum (make-token-seq src (empty))))
|#
