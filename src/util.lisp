(defpackage scheme-on-llvm.util
  (:use :common-lisp)
  (:export #:index #:nlist? #:curry #:partial #:switch #:alambda #:self))
(in-package :scheme-on-llvm.util)


(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (declare (optimize (space 3)))
  (check-type n index)
  (if (zerop n)
      (endp list)
      (nlist? (1- n) (rest list))))

(defun curry (f)
  (lambda (&rest args)
    (lambda (&rest rest-args)
      (apply f (append args rest-args)))))

(defun partial (f &rest args)
  (apply (curry f) args))

(defmacro switch (test keyform &body cases)
  (let ((g!keyform (gensym "KEYFORM")))
    `(let ((,g!keyform ,keyform))
       (cond ,@(mapcar (lambda (case)
                         (destructuring-bind (data . body) case
                           (cond
                                 ((eq data 'otherwise)
                                  `(t ,@body))
                                 ((atom data)
                                  `((funcall ,test ,g!keyform ,data)
                                    ,@body))
                                 (t
                                  `((or ,@(mapcar (lambda (datum)
                                                    `(funcall ,test ,g!keyform ,datum))
                                                  data))
                                    ,@body)))))
                       cases)))))

(defmacro alambda (args &body body)
  `(labels ((self ,args ,@body)) #'self))
